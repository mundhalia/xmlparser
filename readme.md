# XmlParser

[![Latest Version on Packagist][ico-version]][link-packagist]
[![Total Downloads][ico-downloads]][link-downloads]
[![Build Status][ico-travis]][link-travis]

## Installation

Via Composer

``` bash
$ composer require mundhalia/xmlparser
```

## Usage

For usage xmlParaser extends and wraps laravel friendly package for xml rendering.

__config/app.php__

```php
'providers' => [
    Mundhalia\XmlParser\XmlParserServiceProvider::class
]

'aliases' => [
    'XmlParser' => Mundhalia\XmlParser\Facades\XmlParser::class
]
```

For usage in a function:

```php
use XmlParser;

public function index(){
    XmlParser::render($xml); //will return same xml as supplied
    XmlParser::render($xml, 'collection'); //will return laravel collection object
    XmlParser::render($xml, 'array'); //will return larray
    XmlParser::render($xml, 'object'); //will return object
}
```

## Security

If you discover any security related issues, please email author email instead of using the issue tracker.

## Credits

- [Aditya Mundhalia][link-author]

## License

license. Please see the [license file](license.md) for more information.

[ico-version]: https://img.shields.io/packagist/v/mundhalia/xmlparser.svg?style=flat-square
[ico-downloads]: https://img.shields.io/packagist/dt/mundhalia/xmlparser.svg?style=flat-square
[ico-travis]: https://img.shields.io/travis/mundhalia/xmlparser/master.svg?style=flat-square
[ico-styleci]: https://styleci.io/repos/12345678/shield
[link-packagist]: https://packagist.org/packages/mundhalia/xmlparser
[link-downloads]: https://packagist.org/packages/mundhalia/xmlparser
[link-travis]: https://travis-ci.org/mundhalia/xmlparser
[link-styleci]: https://styleci.io/repos/12345678
[link-author]: https://github.com/aadityamundhalia
[link-contributors]: ../../contributors