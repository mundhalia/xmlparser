<?php

namespace Mundhalia\XmlParser\Facades;

use Illuminate\Support\Facades\Facade;

class XmlParser extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'xmlparser';
    }
}
