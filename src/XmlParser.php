<?php

namespace Mundhalia\XmlParser;

class XmlParser
{
    // Build wonderful things
    public function __construct()
    {
    
    }

    public function render($xml, $type = null)
    {
        $xmlObj = simplexml_load_string($xml);
        $json = json_encode($xmlObj);
        switch ($type) {
            case "collection":
                $array = json_decode($json);
                $collection = collect([$array]);
                return $collection;
                break;
            case "array":
                $array = json_decode($json, true);
                return $array;
                break;
            case "json":
                return $json;
                break;
            case "object":
                return $xmlObj;
                break;
            default:
                return $xml;
        }
    }
}