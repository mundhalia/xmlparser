<?php

namespace Mundhalia\XmlParser;

use Illuminate\Support\ServiceProvider;

class XmlParserServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        // $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'mundhalia');
        // $this->loadViewsFrom(__DIR__.'/../resources/views', 'mundhalia');
        // $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        // $this->loadRoutesFrom(__DIR__.'/routes.php');

        // Publishing is only necessary when using the CLI.
        if ($this->app->runningInConsole()) {
            $this->bootForConsole();
        }
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/xmlparser.php', 'xmlparser');

        // Register the service the package provides.
        $this->app->singleton('xmlparser', function ($app) {
            return new XmlParser;
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['xmlparser'];
    }
    
    /**
     * Console-specific booting.
     *
     * @return void
     */
    protected function bootForConsole()
    {
        // Publishing the configuration file.
        $this->publishes([
            __DIR__.'/../config/xmlparser.php' => config_path('xmlparser.php'),
        ], 'xmlparser.config');

        // Publishing the views.
        /*$this->publishes([
            __DIR__.'/../resources/views' => base_path('resources/views/vendor/mundhalia'),
        ], 'xmlparser.views');*/

        // Publishing assets.
        /*$this->publishes([
            __DIR__.'/../resources/assets' => public_path('vendor/mundhalia'),
        ], 'xmlparser.views');*/

        // Publishing the translation files.
        /*$this->publishes([
            __DIR__.'/../resources/lang' => resource_path('lang/vendor/mundhalia'),
        ], 'xmlparser.views');*/

        // Registering package commands.
        // $this->commands([]);
    }
}
